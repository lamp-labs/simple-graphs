Gem::Specification.new do |s|
  s.name        = 'simple_graphs'
  s.version     = '0.8.1'
  s.license     = 'MIT'
  s.summary     = 'Simple Graphs gem provide easy to use API to create and work with graphs'
  s.description = 'Simple Graphs gem provide easy to use API to create and work with graphs. Now supports only adjacency lists.'
  s.author      = 'Sergey Larin'
  s.email       = 's.larin@lamp-labs.ru'
  s.homepage    = 'https://gitlab.com/lamp-labs/simple-graphs'
  s.files       = Dir['lib/**/*.rb']

  s.add_runtime_dependency 'full_clone', ['= 0.0.4']
  s.add_runtime_dependency 'abstract', ['= 1.0.0']
  s.add_runtime_dependency 'interface', ['= 1.0.3']
end