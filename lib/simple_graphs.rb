require 'SimpleGraphs/version'

module SimpleGraphs
  require 'SimpleGraphs/Graph/adjacency_lists'
  require 'SimpleGraphs/Graph/graph_modules'
  require 'SimpleGraphs/Algorithm/algorithm_modules'
  require 'SimpleGraphs/Algorithm/search'
end