require 'interface'

module Algorithm
  BASE = interface{
    required_methods :run
  }

  module Controllable

    def add_criteria(type, &condition)
      (criteria[type] ||= []) << condition
    end

    private
    def check_criteria(type, from_vertex, to_vertex, edge_weight, callback_result)
      criteria[type].inject(false) do |result, condition|
        result || condition.call(from_vertex, to_vertex, edge_weight, callback_result)
      end if criteria[type]
    end

    def criteria
      @criteria ||= {}
    end
  end
end