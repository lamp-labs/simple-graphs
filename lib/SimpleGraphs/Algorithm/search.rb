require_relative 'algorithm_modules'

module Algorithm
  class Search
    include Controllable

    # @param g [Graph]
    # @param from_vertex [String] vertex with which to start search
    # @param handle [Proc] block is executed at the entrance vertex_to the node
    def run(g, from_vertex, direction, &handle)
      ignore_nodes = Set.new
      stack = [{child: from_vertex, parent: nil}]
      until stack.empty?
        top_pair = case direction
                     when :dfs
                       stack.pop
                     when :bfs
                       stack.shift
                     else
                       raise 'Unsupported direction'
                   end
        _child = top_pair[:child]
        _parent = top_pair[:parent]
        callback_result = g.execute_callback(_child)
        unless ignore_nodes.include? _child
          edge_weight = g.weight_for(_parent, _child)
          handle.call _parent, _child, callback_result, edge_weight if handle
          ignore_nodes.add _child
          if check_criteria(:stop, _parent, _child, callback_result, edge_weight)
            ignore_nodes.merge g.vertices
          end
          unless check_criteria(:skip_children, _parent, _child, callback_result, edge_weight)
            stack += g.children_for(_child).map { |child| {child: child, parent: _child} }
          end
        end
      end
    end

    implements BASE
  end
end