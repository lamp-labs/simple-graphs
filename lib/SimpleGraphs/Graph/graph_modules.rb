require 'abstract'

module Graph
  class Base
    abstract_method '', :empty?, :cycles, :edges, :outs, :enters, :disjointed
    abstract_method 'vertex', :has_vertex?, :children_for
    abstract_method 'vertex_from, vertex_to', :has_edge?
    abstract_method 'vertex_from, edges', :add_edges
    abstract_method 'src, dest = nil', :remove_edges

    def vertices
      outs + enters + disjointed
    end

    # @return [Set] the set of nodes in which are not enter dest
    def sources
      disjointed + outs - enters
    end

    # @return [Set] the set of nodes of which are not out dest
    def runoffs
      disjointed + enters - outs
    end
  end

  module Adjustable

    def get_property(property)
      graph_settings[property]
    end

    def set_property(property, value)
      graph_settings[property] = value
    end

    private
    def graph_settings
      @graph_settings ||= {}
    end
  end

  module WeighedNodes

    # @param vertex_name [String]
    # @param vertex_callback [Object] block related on vertex
    def add_vertex_callback (vertex_name, &vertex_callback)
      hooked_successfully = has_vertex?(vertex_name) && !vertex_callbacks.has_key?(vertex_name)
      if hooked_successfully
        vertex_callbacks[vertex_name] = vertex_callback if vertex_callback
      end
      hooked_successfully
    end

    # @param vertex_name [String] name for starting_vertex
    def remove_vertex_callback (vertex_name)
      vertex_callbacks.delete vertex_name
    end

    def execute_callback (vertex_name)
      vertex_callbacks[vertex_name] ? vertex_callbacks[vertex_name].call : nil
    end

    private
    def vertex_callbacks
      @vertex_callbacks ||= {}
    end
  end
end