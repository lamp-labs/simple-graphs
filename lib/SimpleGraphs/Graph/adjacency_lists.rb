require 'full_clone'
require_relative 'graph_modules'

module Graph
  class AdjacencyLists < Base
    include Adjustable
    include WeighedNodes

    # graph.new {acyclic: true, ...}
    # @param settings [Hash] settings symbols
    def initialize(settings = {})
      settings.each { |property, value| set_property(property, value)}
      @edges = {}
    end

    def has_edge?(vertex_from, vertex_to)
      edges.include? [vertex_from, vertex_to]
    end

    def has_vertex?(vertex)
      vertices.include? vertex
    end

    # @param from_vertex [String] vertex for starting_vertex
    # @param target_vertexes [Hash] pairs in format <vertex_to: edge_weight>
    # @return [Array] of dest vertices if the insert does not cause the loop when acyclic: true
    def add_edges(from_vertex, target_vertexes = {})
      edges_before_addition = self.edges
      unless has_vertex?(from_vertex) && target_vertexes.empty?
        @edges.each { |src, trgs| remove_edges src if trgs.empty? && target_vertexes.has_key?(src)}
        merge_edges from_vertex, target_vertexes
        remove_cycles_with(from_vertex)
      end
      added_edges = self.edges - edges_before_addition
      added_edges.empty? ? nil : added_edges
    end

    # @param src [String] vertex for starting_vertex vertex_to remove
    # @param dest [Array] array of final_vertex names vertex_to remove
    def remove_edges(src, dest = nil)
      if dest.nil?
        @edges.delete src
      elsif @edges[src]
        if dest.kind_of?(Array)
          dest.each { |e| @edges[src].delete e }
        else
          @edges[src].delete dest
        end
        @edges.delete src if @edges[src].empty? || dest.kind_of?(Array) && dest.empty?
      end
      vertex_callbacks.delete_if { |v, _| !vertices.include? v }
    end

    # @param vertex [String] vertex of starting_vertex
    # @return [Array] of children vertices
    def children_for(vertex)
      outs_for(vertex).keys
    end

    def edges
      @edges.inject([]) do |hash, (src, edges)|
        hash + edges.keys.inject([]) { |h, (dest, _)| h << [src, dest] }
      end
    end

    # @return [Set] the set of outs
    def outs
      Set.new(@edges.select { |_, to_set| !to_set.empty? }.map { |from, _| from })
    end

    # @return [Set] the set of enters
    def enters
      Set.new(@edges.values.collect { |v| v.keys }.flatten)
    end

    # @return [Set] the set of nodes in which are not enter dest
    def sources
      disjointed + outs - enters
    end

    # @return [Set] the set of nodes of which are not out dest
    def runoffs
      disjointed + enters - outs
    end

    # @return [Set] the set of nodes not connected with other vertices
    def disjointed
      Set.new(@edges.keys) - outs - enters
    end

    # @return [Boolean] true if graph does not contain vertices
    def empty?
      @edges.empty?
    end

    # @return [AdjacencyLists] containing all cycles or nil if it is missing
    def cycles
      c = full_clone
      c.disjointed.each { |v| c.remove_edges v }
      until (sr = c.sources + c.runoffs).empty? || c.empty?
        c.edges.each { |src, dest| c.remove_edges(src, dest) if sr.include?(src) || sr.include?(dest) }
      end
      c.set_property(:acyclic, false)
      c.empty? ? nil : c
    end

    def weight_for(from_vertex, to_vertex)
      @edges[from_vertex] ? @edges[from_vertex][to_vertex] : nil
    end

    def to_s
      @edges.inspect
    end

    private

    def outs_for(vertex)
      @edges[vertex] || {}
    end

    def merge_edges(vertex_from, edges)
      (@edges[vertex_from] ||= {}).merge!(edges) { |_, old, _| old }
    end

    def remove_cycles_with(vertex_from)
      graph_cycled = cycles
      if graph_settings[:acyclic] && graph_cycled
        from_vertex_children = graph_cycled.children_for vertex_from
        remove_edges vertex_from, from_vertex_children
      end
    end
  end
end