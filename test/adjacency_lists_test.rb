require 'test/unit'
require 'rubygems'
require '../lib/SimpleGraphs/Graph/adjacency_lists'
require '../lib/SimpleGraphs/Algorithm/search'

class AdjacencyListsTest < Test::Unit::TestCase

  def setup
    @acyclic_graph = Graph::AdjacencyLists.new({acyclic: true})
    @cyclic_graph = Graph::AdjacencyLists.new
  end

  def test_vertex_addition
    edges = @acyclic_graph.add_edges(:test_vertex, {other_test_vertex: 'edge_data'}) { 'test_vertex_data' }
    assert(!edges.nil? && edges.include?([:test_vertex, :other_test_vertex]), 'Должен быть возвращён список добавленных рёбер')
    assert_send([@acyclic_graph.vertices, :include?, :test_vertex], 'Граф должен содержать исходящюю вершину ребра')
    assert_send([@acyclic_graph.vertices, :include?, :other_test_vertex], 'Граф должен содержать конечную вершину ребра')
    assert(@acyclic_graph.edges.include?([:test_vertex, :other_test_vertex]), 'Граф должен содержать ребро')
  end

  def test_vertex_removal
    @acyclic_graph.add_edges(:first, {second: 'edge_data'}) { 'test_vertex_data' }
    @acyclic_graph.remove_edges(:first, [:second])
    assert(!@acyclic_graph.edges.include?([:first, :second]), 'Ребро должно быть удалено')
    @acyclic_graph.add_edges(:first, {second: 'edge_data'}) { 'test_vertex_data' }
    @acyclic_graph.add_edges(:first, {third: 'edge_data'}) { 'test_vertex_data' }
    @acyclic_graph.remove_edges(:first)
    @acyclic_graph.edges.flatten.include?(:first)
    assert(!@acyclic_graph.edges.include?(:first), 'Все связанные рёбра должны быть удалены')
  end

  def test_acyclic_vertex_addition
    @acyclic_graph.add_edges(:first, {second: nil})
    @acyclic_graph.add_edges(:second, {third: nil})
    assert(!@acyclic_graph.edges.include?([:third, :first]), 'Обратное ребро не должно быть добавлено')
  end

  def test_acyclic_self_edges
    assert(!@acyclic_graph.add_edges(:first, {first: nil}), 'При безуспешном добавлении ребра должна быть возвращена ложь')
    assert(!@acyclic_graph.edges.include?([:first, :first]), 'Обратное ребро не должно быть добавлено')
  end

  def test_double_edges
    assert_not_nil(@acyclic_graph.add_edges(:first, {second: 'edge_data'}), 'add_edges должен вернуть список добавленных рёбер')
    assert_nil(@acyclic_graph.add_edges(:first, {second: 'edge_data'}), 'В графе не должно быть дублирующих рёбер')
  end

  def test_search
                                                          #   +--1--+--1
    @acyclic_graph.add_edges(:_0, {_1: 1, _2: 1, _3: 1})  #   |     |
    @acyclic_graph.add_edges(:_1, {_1_1: 2, _1_2: 2})     # 0-+--2  +--2
    @acyclic_graph.add_edges(:_3, {_3_1: 2})              #   |
                                                          #   +--3-----1
    @acyclic_graph.add_vertex_callback(:_0) { :_0 }
    @acyclic_graph.add_vertex_callback(:_2) { :_2 }
    @acyclic_graph.add_vertex_callback(:_3_1) { :_3_1 }
    @acyclic_graph.add_vertex_callback(:_3) { :_3 }
    @acyclic_graph.remove_vertex_callback(:_3)
    tab_order = []
    call_order = []
    Algorithm::Search.new.run(@acyclic_graph, :_0, :dfs) do |_, n, cr, _|
      tab_order << n
      call_order << cr
    end
    assert_equal(call_order, [:_0, nil, :_3_1, :_2, nil, nil, nil], 'Нарушен порядок вызова коллбеков при обходе в глубину')
    assert_equal(tab_order, [:_0, :_3, :_3_1, :_2, :_1, :_1_2, :_1_1], 'Нарушен порядок обхода в глубину')

    tab_order = []
    call_order = []
    Algorithm::Search.new.run(@acyclic_graph, :_0, :bfs) do |_, n, cr, _|
      tab_order << n
      call_order << cr
    end
    assert_equal(call_order, [:_0, nil, :_2, nil, nil, nil, :_3_1], 'Нарушен порядок вызова коллбеков при обходе в ширину')
    assert_equal(tab_order, [:_0, :_1, :_2, :_3, :_1_1, :_1_2, :_3_1], 'Нарушен порядок обхода в ширину')
  end

  def test_cycles
    @cyclic_graph.add_edges(:_0, {})
    assert(@cyclic_graph.cycles.nil?, 'Граф не должен быть циклическим')
    @cyclic_graph.add_edges(:_0, {_1: 1, _2: 1, _3: 1})
    @cyclic_graph.add_edges(:_1_1)
    @cyclic_graph.add_edges(:_1, {_1_1: 2, _1_2: 2})
    @cyclic_graph.add_edges(:_1_2, {_0: 2})
    assert(!@cyclic_graph.cycles.nil?, 'Граф должен быть циклическим')
  end

  def test_callbacks
    test_array = []
    @acyclic_graph.add_edges(:_0, {_1: 1})
    @acyclic_graph.add_vertex_callback(:_0) { test_array << :_0 }
    @acyclic_graph.add_vertex_callback(:_1) { test_array << :_1 }
    assert(!test_array.include?(:_0) && !test_array.include?(:_1))
    Algorithm::Search.new.run(@acyclic_graph, :_0, :dfs)
    assert(test_array.include?(:_0) && test_array.include?(:_1))
  end

  def test_skip_children_criteria
    tab_order = []

    @acyclic_graph.add_edges(:_0, {_1: 1, _2: 1, _3: 1})
    @acyclic_graph.add_edges(:_1, {_1_1: 2, _1_2: 2})
    @acyclic_graph.add_edges(:_3, {_3_1: 2})

    dfs = Algorithm::Search.new
    dfs.add_criteria(:skip_children) { |_, to, _, _| to == :_1 }
    dfs.run(@acyclic_graph, :_0, :dfs) { |_, to, _, _| tab_order << to }

    assert_equal(tab_order, [:_0, :_3, :_3_1, :_2, :_1], 'Нестработал критерий :skip_children')
  end

  def test_stop_criteria
    tab_order = []

    @acyclic_graph.add_edges(:_0, {_1: 1, _2: 1, _3: 1})
    @acyclic_graph.add_edges(:_1, {_1_1: 2, _1_2: 2})
    @acyclic_graph.add_edges(:_3, {_3_1: 2})

    dfs = Algorithm::Search.new
    dfs.add_criteria(:stop) { |_, to, _, _| to == :_1_2 }
    dfs.run(@acyclic_graph, :_0, :dfs) { |_, to, _, _| tab_order << to }

    assert_equal(tab_order, [:_0, :_3, :_3_1, :_2, :_1, :_1_2], 'Нестработал критерий :stop')
  end

  def test_vertexes
                                                          #   +--1--+--1
    @acyclic_graph.add_edges(:_0, {_1: 1, _2: 1, _3: 1})  #   |     |
    @acyclic_graph.add_edges(:_1, {_1_1: 2, _1_2: 2})     # 0-+--2  +--2
    @acyclic_graph.add_edges(:_3, {_3_1: 2})              #   |
    @acyclic_graph.add_edges(:_4)                         #   +--3-----1
                                                          # 4
    assert_equal(@acyclic_graph.vertices, Set.new([:_0, :_1, :_2, :_3, :_4, :_1_1, :_1_2, :_3_1]))
    assert_equal(@acyclic_graph.sources, Set.new([:_0, :_4]))
    assert_equal(@acyclic_graph.runoffs, Set.new([:_2, :_4, :_1_1, :_1_2, :_3_1]))
    assert_equal(@acyclic_graph.disjointed, Set.new([:_4]))
    assert_equal(@acyclic_graph.outs, Set.new([:_0, :_1, :_3]))
    assert_equal(@acyclic_graph.enters, Set.new([:_1, :_2, :_3, :_1_1, :_1_2, :_3_1]))
  end
end